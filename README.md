# Auth2faProject

Ce projet implémente un système d'authentification basique en Go.\
Il utilise des tokens pour authentifier les utilisateurs et protéger l'accès à des ressources spécifiques. \
Le système génère des tokens sécurisés qui peuvent être utilisés pour valider les requêtes des utilisateurs.

## Les branches

Dans ce projet il y a 4 branches: \
Main : Le Readme. \
Master : La base du projet demandé. \
Dev : Les améliorations possibles. \
Feature/2fa-authentification : La mise ne place de l'authentification avec QRCode.

## Fonctionnalités
Génération de Token: Génère des tokens uniques pour les utilisateurs.

Validation de Token: Valide les tokens envoyés par les utilisateurs pour accéder à des ressources protégées.

Nettoyage Automatique: Supprime automatiquement les tokens expirés pour maintenir le système propre et sécurisé.

## Configuration
Pour configurer et exécuter ce projet sur votre système local, suivez ces étapes :

### Prérequis : 
Assurez-vous d'avoir Go installé sur votre système. Vous pouvez le télécharger et l'installer à partir de golang.org.

### Clonez le Répertoire : 
Clonez ce projet dans votre espace de travail Go en utilisant la commande suivante :

```
git clone https://gitlab.com/CGERARD35/auth2faproject.git
```

### Localisation :
Le code de base se trouve sur la branche master.\
Pour ce qui est des fonctionnalités additionnelles, elles se trouve sur la branche dev.

### Installation des Dépendances : 
Accédez au dossier du projet et installez les dépendances nécessaires :
```
cd yourprojectname
go mod tidy
```
### Lancement des Tests
Ce projet comprend des tests unitaires et d'intégration pour vérifier la fonctionnalité du système d'authentification. Pour lancer les tests, utilisez la commande suivante dans le dossier du projet :

```
go test -v ./...
```
Cela exécutera tous les tests dans le projet et affichera les résultats dans le terminal.

## Démarrage de l'Application
Pour démarrer les serveurs d'authentification et de ressources directement à partir du main.go, suivez ces étapes :

### Démarrer le Serveur : 
Exécutez la commande suivante dans le terminal à la racine du projet :
```
go run main.go
```

Cette commande compile et exécute le code dans main.go, qui initialise et démarre les serveurs d'authentification et de ressources. Par défaut, le serveur d'authentification écoute sur le port 9000 et le serveur de ressources sur le port 9001.

### Vérifier le Fonctionnement : 
Vous pouvez vérifier que les serveurs fonctionnent correctement en envoyant une requête HTTP à l'aide de curl ou de tout autre outil de requête HTTP, comme Postman.

Après avoir démarrer les serveurs, rendez vous sur l'adresse suivante afin de générer un token pour l'utilisateur (ID=3) :
```
http://localhost:9000/generate?userID=3
```
Vous devriez recevoir un token en réponse. Vous pouvez ensuite utiliser ce token pour accéder à une ressource protégée sur le serveur de ressources.

Copiez le token généré puis entrez l'adresse suivante pour vérifier que le token est valide en le copiant à la place de < TOKEN > (attention vous n'avez que 30s avant que le token n'expire) :
```
http://localhost:9001/protected?userID=3&token=<TOKEN>
```

Un message vous informe de la validité du token.

Structure du Projet
Le projet est structuré comme suit :

client : Contient la logique de requête de l'utilisateur aux différents serveurs \
auth.go : Contient la logique principale pour la génération et la validation des tokens.\
auth_test.go : Contient les tests unitaires pour la logique d'authentification.\
authInt_test.go : Contient le test d'intégration pour l'accès aux ressources protégées.\
handlers.go : Définit les gestionnaires HTTP pour les routes d'authentification.\
main.go : Le point d'entrée du serveur HTTP.